import React from 'react';
import { BrowserRouter, Route , Switch, Link } from 'react-router-dom';


import Login from './Pages/Login';
import Recover from './Pages/Recover';
import test from './Pages/test';
import Geosites from './Pages/Geosites';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/recover" component={Recover} />
        <Route path="/test" component={test} />
        <Route path="/geosites" component={Geosites} />
      </Switch>
    </BrowserRouter>
  );
}