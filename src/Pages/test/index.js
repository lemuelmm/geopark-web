import React, {useState, useEffect} from 'react';
import api from '../../services/api';

import '../Login/styles.css';


export default function () {
  const [list, setlist] = useState();

  useEffect(async () => {
    const response = await api.get('/users')
    setlist(response.data)
  })

  const test = () => {
    for (const key in list) {
     console.log(list[key])
    }
  }

  return (
    <div>
     {test()}
    </div>
  )
}