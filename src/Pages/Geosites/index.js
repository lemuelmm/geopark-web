import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './styles.css';

import logo from '../../Assets/logo.svg';


// Each logical "route" has two components, one for
// the sidebar and one for the main area. We want to
// render both of them in different places when the
// path matches the current URL.

// We are going to use this route config in 2
// spots: once for the sidebar and once in the main
// content section. All routes are in the same
// order they would appear in a <Switch>.
const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div>home!</div>,
    main: () => <h2>Home</h2>
  },
  {
    path: "/bubblegum",
    sidebar: () => <div>bubblegum!</div>,
    main: () => <h2>Bubblegum</h2>
  },
  {
    path: "/shoelaces",
    sidebar: () => <div>shoelaces!</div>,
    main: () => <h2>Shoelaces</h2>
  }
];

export default function Geosites() {
  return (
    <Router>
      <div style={{ display: "flex" }}>
        <div className="side-bar">
          <div className="logoGeopark">
            <img  alt="Logo Geopark" src={logo} />
            <p> Geopark Araripe </p>
          </div>
          <div className="linkList">
            <ul style={{ listStyleType: "none", padding: 0 }}>

              <div className="section">
                <h2>Solicitações</h2>
                <li>
                  <Link to="/">Criar solicitações</Link>
                </li>
                <li>
                  <Link to="/">Ver solicitações em aberto</Link>
                </li>
              </div>
              
              <div className="section">
                <h2>Relatórios</h2>
                <li>
                  <Link to="/">Relatórios dos Geosítios</Link>
                </li>
                <li>
                  <Link to="/">Relatórios do Geopark</Link>
                </li>
              </div>

              <div className="section">
                <h2>Solicitações</h2>
                <li>
                  <Link to="/">Lista de pesquisadores</Link>
                </li>
                <li>
                  <Link to="/">Adicionar pesquisador</Link>
                </li>
                <li>
                  <Link to="/">Gerenciar geosítios</Link>
                </li>
                <li>
                  <Link to="/">Adicionar geosítios</Link>
                </li>
              </div>

              <li className="sair" >
                <Link to="/">Confirgurações</Link>
              </li>
            </ul>
          </div>

          <Switch>
            {routes.map((route, index) => (
              // You can render a <Route> in as many places
              // as you want in your app. It will render along
              // with any other <Route>s that also match the URL.
              // So, a sidebar or breadcrumbs or anything else
              // that requires you to render multiple things
              // in multiple places at the same URL is nothing
              // more than multiple <Route>s.
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                children={<route.sidebar />}
              />
            ))}
          </Switch>
        </div>

        <div style={{ flex: 1, padding: "10px" }}>
          <Switch>
            {routes.map((route, index) => (
              // Render more <Route>s with the same paths as
              // above, but different components this time.
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                children={<route.main />}
              />
            ))}
          </Switch>
        </div>
      </div>
    </Router>
  );
}
