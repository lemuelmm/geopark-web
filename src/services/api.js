import axios from 'axios';

const api = axios.create({
  baseURL: 'https://servergeopark.herokuapp.com',
  headers: {
    Accept: 'application/json, text/plain, */*',
    contentType: 'application/json, */*; charset=utf-8',
  },
});

api.interceptors.request.use(
  async config => {
    try {
      const token = await localStorage.getItem('@GeoParkWeb:token');
      if(token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    } catch(error) {
      return config;
    }
  },
  
  error => {
    console.log(error)
    return error;
  }
)

export default api;